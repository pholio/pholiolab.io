// Give iframe some time to load, while re-checking
define(function (require) {
  const { APP_URL } = require('./config');
  var retriesLeft = 30;

  function checkAuthentication() {
    retriesLeft--;

    console.log(`checking if authenticated...`);
    var iframe = document.querySelector('.appWidget');
    if (iframe.contentWindow.length) {
      $('.lock-box').addClass('d-none');
      iframe.classList.remove('d-none')
      clearInterval(authChecker);
    } else if (retriesLeft <= 0) {
      console.log(`app has failed to load, exhausted all retries.`);
      $('#loader').addClass('no-visible');
      $('#authorization').removeClass('no-visible');
      clearInterval(authChecker);
    } else {
      console.log(`app has not loaded yet.`);
    }
  }

  $('.appWidget').attr('src', APP_URL);
  window.authChecker = setInterval(checkAuthentication, 200);
  document.querySelector('#authorize').addEventListener('click', function (event) {
    var currentUrl = document.location.href;
    var authPage = `${APP_URL}?auth=true&redirect=${encodeURIComponent(currentUrl)}`;
    console.log(`Sending user for authorization: ${authPage}`)
    window.open(authPage, '_blank');
  });
});
